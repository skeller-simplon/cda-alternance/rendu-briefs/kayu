# Kayu

## Brief
> À l'aide de React Native et Expo, l'objectif est de créer une application mobile qui nous permettrait de scanner des code-barres de produits alimentaires pour obtenir des informations sur la qualité du produit.

### Ressources
- [Figma](https://www.figma.com/file/XV41137YUGagFbp3fSCPDo/KAYU-APP?node-id=0%3A1)
- [Logo SVG](https://gist.github.com/clementcauser/d8cea60eea1e72937f23ef9dc27631a6)
- [API - OpenFoodFacts](https://fr.openfoodfacts.org/)
- [Doc Expo](https://docs.expo.io/)


## Réalisations
    1. Un écran de scan qui utilise webcam pour scanner un code-barre et le chercher sur OpenFoodFacts. Cet ecran redirige ensuite sur les détails du produit.
    2. Un écran de recherche qui fait des requêtes à l'api d'OpenFoodFacts depuis un input. On peut ensuite afficher les détails d'un produit ou d'un autre.
    3. Un écran d'historique ou sont affichés tous les produits dont on a déjà vu les détails. Ils sont stockés via l'async storage (https://react-native-async-storage.github.io/async-storage/)

## Screens

![Historique screen](doc/history_screen.jpg "Historique screen")
![Scan Screen](doc/scan_screen.jpg "Scan Screen")
![Search results](doc/search_results.jpg "Search results")