import axios from "axios";


axios.defaults.headers.common = {
    "User-Agent": "SimplonSchoolTraining - Android - Version 1.0 - https://gitlab.com/samuelkeller",
  };
  

function getInfosByCodeProduit(codeProduit: string) {
    return axios.get<FullProduct>("https://world.openfoodfacts.org/api/v0/product/" + codeProduit + ".json")
}

function searchByName(search: string)  {
    return axios.get<SearchResult>(`https://world.openfoodfacts.org/cgi/search.pl?search_terms=${search}&search_simple=1&action=process&page_size=20&json=1`)  
}

export { getInfosByCodeProduit,searchByName };
