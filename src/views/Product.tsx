import React, { useEffect, useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import Card from "react-native-elements/dist/card/Card";
import { ScrollView } from "react-native-gesture-handler";
import IngredientList from "../components/IngredientList";
import Layout from "../components/shared/Layout";

type ProductProps = {
  route: any;
};
export default function Product({ route }: ProductProps) {
  const [productData, setProductData] =
    useState<FullProduct | undefined>(undefined);

  useEffect(() => {
    setProductData(route.params.product);
  }, [productData]);

  return (
    <>
      {productData && (
        <Layout title="Product">
          <ScrollView nestedScrollEnabled>
            <Card>
              <Card.Title>{productData.product.product_name}</Card.Title>
              <Card.Image source={{ uri: productData.product.image_url }} />
              <Card.Divider />
              <View>
                {!!productData.product.nutriscore_grade && (
                  <Text>
                    Nutriscore : {productData.product.nutriscore_grade}
                  </Text>
                )}
                {!!productData.product.origins && (
                  <Text>Origine : {productData.product.origins}</Text>
                )}
                {!!productData.product.ingredients && productData.product.ingredients.length > 1 ? (
                  <IngredientList
                    ingredients={productData.product.ingredients}
                  />
                ) : (
                  <Text>Pas d'ingrédients connus.</Text>
                )}
              </View>
            </Card>
          </ScrollView>
        </Layout>
      )}
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "flex-start",
    backgroundColor: "#fff",
  },
  image: {
    width: "50%",
    height: 200,
  },
  barCodeView: {
    width: "100%",
    height: "50%",
    marginBottom: 40,
  },
});
