import React, { useEffect } from "react";
import {
  StyleSheet,
  FlatList,
  TextInput,
  View,
  Image,
  Text,
} from "react-native";
import IconButton from "../components/shared/IconButton";
import Layout from "../components/shared/Layout";
import { searchByName } from "../services/OpenFoodFactsApi";
import { MaterialIcons } from "@expo/vector-icons";
import BubbleContainer from "../components/shared/BubbleContainer";
import { useNavigation } from "@react-navigation/core";
import { Routes } from "../navigation/constants";
import { addProductToStorage } from "../store/HistoryStore";
import ProductSources from "../store/constants/ProductSources";

export default function Search() {
  const [inputText, onChangeInputText] = React.useState("");
  const [results, setResults] = React.useState<SearchResult | null>(null);
  const { navigate } = useNavigation();

  const searchResults = () => {
    searchByName(inputText).then((resp) => {
      setResults(resp.data);
    });
  };

  const navigateToProduct = (product: Product) => {
    let param = { product, code: product._id } as FullProduct;
    addProductToStorage(param, ProductSources.SEARCH).then((res) => {
      navigate(Routes.SCAN, {
        screen: Routes.PRODUCT,
        params: { product: param },
      });
    });
  };

  return (
    <Layout title="Search">
      <View style={styles.formContainer}>
        <TextInput
          style={styles.input}
          onChangeText={onChangeInputText}
          value={inputText}
        />
        <IconButton onPress={searchResults}>
          <MaterialIcons name="search" size={24} color="black" />
        </IconButton>
      </View>
      {!!results && results.products.length > 0 && (
        <FlatList
          data={results.products}
          keyExtractor={(item) => "search" + item._id}
          renderItem={({ item }: {item: Product}) => (
            <BubbleContainer>
              <Image style={styles.image} source={{ uri: item.image_url }} />
              <View>
                <Text>{item.product_name}</Text>
                <Text style={styles.subtext}>
                  {item.ingredients ? item.ingredients.length : "0"} Ingredients
                </Text>
              </View>
              <IconButton onPress={() => navigateToProduct(item)}>
                <MaterialIcons name="navigate-next" size={24} color="black" />
              </IconButton>
            </BubbleContainer>
          )}
        />
      )}
    </Layout>
  );
}

const styles = StyleSheet.create({
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    flex: 10,
    backgroundColor: "white",
  },
  image: {
    width: 50,
    height: 50,
  },
  formContainer: {
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    borderColor: "#E3E3E3",
    borderWidth: 1,
    marginBottom: 10,
    backgroundColor: "#F1F1F1",
    padding: 10,
    borderRadius: 10,
  },
  subtext: {
    color: "grey",
    fontSize: 10,
  },
});
