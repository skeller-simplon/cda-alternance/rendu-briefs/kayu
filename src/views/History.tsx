import React, { useEffect, useState } from "react";
import {
  FlatList,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from "react-native";
import IconButton from "../components/shared/IconButton";
import Layout from "../components/shared/Layout";
import { getProductsFromStorage } from "../store/HistoryStore";
import { MaterialIcons } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/core";
import { Routes } from "../navigation/constants";
import BubbleContainer from "../components/shared/BubbleContainer";
import { StoreProduct } from "../types/store/StoreProduct";
import ProductSources from "../store/constants/ProductSources";

export default function History() {
  const [ingredients, setIngredients] = useState<StoreProduct[]>([]);
  const { navigate } = useNavigation();

  useEffect(() => {
    getProductsFromStorage().then((ingredients) => setIngredients(ingredients));
  });

  const navigateToProduct = (product: FullProduct) => {
    navigate(Routes.SCAN, { screen: Routes.PRODUCT, params: { product } });
  };

  const getSourceText = (product: StoreProduct) => {
    switch (product.source) {
      case ProductSources.SCAN:
        return `Scanné le ${product.date.toString()}`;
      case ProductSources.SEARCH:
        return `Recherché le ${product.date.toString()}`;
      default:
        return "";
    }
  };

  return (
    <Layout title="Historique">
      <ScrollView nestedScrollEnabled>
        <FlatList
          data={ingredients}
          keyExtractor={(item) => item.product.code}
          renderItem={(data) => {
            return (
              <BubbleContainer>
                <View>
                  <Image
                    style={styles.image}
                    source={{ uri: data.item.product.product.image_url }}
                  />
                </View>
                <View>
                  <Text>{data.item.product.product.product_name}</Text>
                  <Text style={styles.subtext}>{getSourceText(data.item)}</Text>
                </View>
                <IconButton
                  onPress={() => navigateToProduct(data.item.product)}
                >
                  <MaterialIcons name="navigate-next" size={24} color="black" />
                </IconButton>
              </BubbleContainer>
            );
          }}
        />
      </ScrollView>
    </Layout>
  );
}
const styles = StyleSheet.create({
  image: {
    width: 50,
    height: 50,
  },
  subtext: {
    color: "grey",
    fontSize: 10,
  },
});
