type SearchResult = {
    count: number
    page: number
    page_count: number
    page_size: number
    products: Product[]
}