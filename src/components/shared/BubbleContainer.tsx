import React, { FC } from "react";
import { Pressable, StyleSheet, View } from "react-native";

type Props = {};

const BubbleContainer: FC<Props> = ({ children }) => {
  return <View style={styles.container}>{children}</View>;
};

export default BubbleContainer;

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    borderColor: "#E3E3E3",
    borderWidth: 1,
    marginBottom: 10,
    backgroundColor: "#F1F1F1",
    padding: 10,
    borderRadius: 10,
  },
});
