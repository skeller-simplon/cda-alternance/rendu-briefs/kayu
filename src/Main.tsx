import React from "react";
import MainNavigator from "./navigation";

export default function Main() {
  return (
    <>
      <MainNavigator />
    </>
  );
}
